﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaCanonScript : MonoBehaviour
{
    public GameObject objetivo;
    private void OnTriggerEnter2D(Collider2D collision)
    {

        Destroy(this.gameObject);
        if (collision.gameObject.GetComponent<PersonajeMovement>() != null)
        {
            collision.gameObject.GetComponent<PersonajeMovement>().death(); 
        }
    }
}
